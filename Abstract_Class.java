package Assignments;
//Abstract Class Example

abstract class Bank{
	
	//Abstract Method
	abstract int personalLoanIntrest();
	//Concrete Method
	public void homeLoanIntrest()
	{
		System.out.println("Intrest On Home Loan is 8 % ");
	}
}

class Sbi extends Bank
{

	@Override
	int personalLoanIntrest() {
		// TODO Auto-generated method stub
		return 12;
	}

	@Override
	public void homeLoanIntrest() {
		// TODO Auto-generated method stub
		super.homeLoanIntrest();
	}
	
}

class Pnb extends Bank
{

	@Override
	int personalLoanIntrest() {
		// TODO Auto-generated method stub
		return 17;
	}

	@Override
	public void homeLoanIntrest() {
		// TODO Auto-generated method stub
		System.out.println("Intrest On Home Loan in PNB is 12 % ");
	}
	
}

public class Abstract_Class {
	
	public static void main(String[] args) throws Exception 
	  {
		Bank b;
		b=new Sbi();
		System.out.println("Sbi Personal Loan Intrest "+b.personalLoanIntrest());
		b.homeLoanIntrest();
		b=new Pnb();
		System.out.println("Sbi Personal Loan Intrest "+b.personalLoanIntrest());
		b.homeLoanIntrest();
	  }
	  

	
}
