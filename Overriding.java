package Assignments;
//Method Overriding
//Rules for method overriding:
/*
 * Final,Static,Final methods can not be overridden
 * Abstract Methods must be overridden
 */

class Parent
{
	public void marriage() throws Exception
	{
		System.out.print("Deepika");
		
	}
}

class Virat extends Parent
{
	public void marriage()throws Exception
	{
		System.out.print("Anushka");
		
	}
}
	public class Overriding {

	 public static void main(String[] args) throws Exception 
  { 
		 Parent p= new Virat();
		 p.marriage();
  }
	}
