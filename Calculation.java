package Assignments;

//Program for method overloading
//Method overloading can be done by changing: 
//1.The number of parameters in two methods.
//2.The data types of the parameters of methods.
//3.The Order of the parameters of methods.
public class Calculation{
  
 // private int FirstNum;
 // private int SecNum;
//  private int ThirdNum;
  
  //Default Constructor
  Calculation()
  {
      
  }
  
  //Parameterized Constructor
  // Calculation(int FirstNum,int SecNum,int ThirdNum)
  // {
  //    this.FirstNum=FirstNum;
  //    this.SecNum=SecNum;
  //    this.ThirdNum=ThirdNum;
  // }
  
  //Method Overloading
  //First Method
  public int multi(int x,int y,int z) throws Exception
  {
      return(x+y+z);
  }
  //Second Method Changing of Parameters
  public double multi(double x,double y,double z) throws Exception
  {
      return(x+y+z);
  }
  //Third Method Change in No. of Arg Passed
  public int multi(int x,int y) throws Exception
  {
      return(x+y);
  }
  
  //Fourth Method
  public double multi(double x,double y) throws Exception
  {
      return(x+y);
  }
 //Changing Order of Parameters in Method
 public void pass(String x,int y) throws Exception
  {
      System.out.println("Student Name :" + x + " "
                         + "Id :" + y);
  }
  
  public void pass(int x,String y) throws Exception
  {
      System.out.println("Id :" + x + " "
                         + "Student Name :" + y);
  }
  
 
  public static void main(String[] args) throws Exception {
      //Creating Object
      Calculation c=new Calculation();
      //Calling First Method
      System.out.println(c.multi(1,2,7));
      //Calling Second Method
      System.out.println(c.multi(1.3,5.6,3.5));
      //Calling Third Method
      System.out.println(c.multi(1,2));
      //Calling Fourth Method
      System.out.println(c.multi(1.5,2.5));
      //Calling Fifth Method
      c.pass("Mayur",201);
      c.pass(202,"Kunal");
  
}
}