package Assignments;
//Super/Base Class
class Car {
	
	public void start() {
		
		System.out.println("Car is Starting");
	}
	
	public void stop() {
		
		System.out.println("Car is Stoping");
	}
}
//Derived Class 1 
//Single Inhertance
class Bmw extends Car
	{
	Bmw(){
		super();
	}
		public void speed() {
			System.out.println("BMW Cars have Speed Of 300 Km/Hr");
		}
	}
//Derived Class 2
//Hierarchical Inheritance
class MarutiSuzuki extends Car{
	MarutiSuzuki(){
		super();
	}
	public void speed() {
		System.out.println("Maruti Suzuki Cars have speed of 200 Km/Hr");
	}
}
//Derived Class 3
//Hierarchical Inheritance
class Tata_Motors extends Car{
	Tata_Motors(){
		super();
	}
	public void speed() {
		System.out.println("Tata Motors Cars have speed of 250 Km/Hr");
	}
}
//Derived Class 4 
//Multilevel Inheritance
class Bmw_Racer extends Bmw
{
	Bmw_Racer(){
		super();
	}
	
	public void automaticBreaks() {
		System.out.println("BMW Racer Car have Automatic Breaking System");
	}
}



public class Inheritance_All {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Car c=new Car();
		//c.toString();
		//c.start();
		//c.stop();
		
		//Single Inheritance
		System.out.println("Example of Single Inheritance");
		//UpCasting
		Car b =new Bmw();
		//Parent class methods can be accessible
		b.start();
		b.stop();
		
		// b.speed();
		
		//Multilevel  Inheritance	
		System.out.println("");
		System.out.println("Example of Multiple Inheritance");
		Bmw_Racer br=new Bmw_Racer();
		//Parent Of Parent Class methods can be accessible
		br.start();
		br.stop();
		//Parent class methods can be accessible
		br.speed();
		//Its Own method
		br.automaticBreaks();

		//Hierarchical Inheritance
		//Here BMW,Maruti Suzuki and Tata Motors all extends Same Class
		//So all the class have right to access the Car class
		System.out.println("");
		System.out.println("Example of Hierarchical Inheritance");
		MarutiSuzuki m= new MarutiSuzuki();
		m.start();
		m.stop();
		m.speed();
		
		Tata_Motors t=new Tata_Motors();
		t.start();
		t.stop();
		t.speed();
	
		
		
		
	}

}
