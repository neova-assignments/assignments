package Assignments;
// Interface Example

public interface Bank_Interface {

	float rateOfInterest();  
}

class SbiBank implements Bank_Interface
{

	@Override
	public float rateOfInterest() {
		// TODO Auto-generated method stub
		return 12;
	}
	
}

class PnbBank implements Bank_Interface{

	@Override
	public float rateOfInterest() {
		// TODO Auto-generated method stub
		return 15;
	}
}
	
class TestInterface{	
	public static void main(String[] args) throws Exception 
	  {
		Bank_Interface b;
		b=new SbiBank();
		System.out.println("ROI in SBI: "+b.rateOfInterest());  
		b=new PnbBank();
		System.out.println("ROI in PNB: "+b.rateOfInterest());  
		
		
		
	  }
	  }
	
	
