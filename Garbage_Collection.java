package Assignments;
//Available Collector in JDK

//-XX:+UseSerialGC

//-XX:+UseParallelGC

//-XX:+UseG1GC

//-XX:+UseConcMarkSweepGC
 
//We can use JConsole to monitor garbage collection

//JVM uses Parallel GC by Default
public class Garbage_Collection {
	private int a;
	private int b;
	 
	  public void operations(int c,int d){
	    a=c;
	    b=d;
	  }
	  
	  public void display(){
	    System.out.println("Value of a = "+a);
	    System.out.println("Value of b = "+b);
	  }
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Garbage_Collection gc1=new Garbage_Collection();
		Garbage_Collection gc2=new Garbage_Collection();
		gc1.operations(1, 2);
		gc1.display();
		gc2.operations(3, 4);
		gc2.display();
		//Both the Objects have ref's
		Garbage_Collection gc3;
		gc3=gc2;
		gc3.display();
		//gc2 is unreferenced applicable of gc
	    gc2=null;
	    gc3.display();
		//gc3 is unreferenced applicable of gc
	    gc3=null;
	  //  gc3.display();
		
	}

}
